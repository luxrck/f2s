LDFLAGS		:= `pkg-config --libs zlib` -lsqlite3
LDFLAGS		+= -static-libstdc++
CPPFLAGS	:= -std=c++11 -Wall -fopenmp -g3

%: %.c
	g++ $(CPPFLAGS) -c $<
	ld -o $@ $(LDFLAGS)
	objdump -S $@ > $@.asm

all: trans