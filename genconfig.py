#!/usr/bin/python2

import sys
import pdb
import argparse
import MySQLdb as mysqldb

DB_HOST = "192.168.1.192"
DB_PORT = 3306
DB_NAME = "dp_idx"
DB_USER = "minishop"
DB_PSWD = "MiniShop!@#"

parser = argparse.ArgumentParser(prog="genconfig", description="Generate MFile list")
parser.add_argument("site_id", nargs=1, \
	help="select where site_id=site_id")
parser.add_argument("-si", "--start_id", nargs="?", \
	help="select where id > start_id")
parser.add_argument("-st", "--start_time", nargs="?", \
	help="select where update_time > start_time")
parser.add_argument("-v", "--verbose", action="store_true", \
	help="print verbosely")
#parser.add_argument("--sort", nargs=1, default=["id"], choices=["id", "time"], \
#	help="sort order by {id(default)|update_time} desc")

if __name__ == "__main__":
	args = parser.parse_args(sys.argv[1:])
	conn = mysqldb.connect(host=DB_HOST, db=DB_NAME, user=DB_USER, passwd=DB_PSWD)
	
	site_id = args.site_id[0]
	sql = "select id,data_file,update_time from data_file where site_id={0} {1} order by id desc"
	
	codi = ""
	if args.start_id: codi = "and id>" + args.start_id
	if args.start_time: codi  = codi + " and update_time>'{0}'".format(args.start_time)

	#sc = args.sort[0]
	#if not sc in ("id", "time"): sc = "id"
	#if sc == 'time': sc = 'update_time'
	sql = sql.format(site_id, codi)
	#print(sql)

	cursor = conn.cursor()
	cursor.execute(sql)
	for row in cursor.fetchall():
		if args.verbose:
			print(row[0], row[1], str(row[2]))
		else:
			print(row[1])