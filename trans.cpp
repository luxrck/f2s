#include <tuple>
#include <mutex>
#include <queue>
#include <vector>
#include <thread>
#include <utility>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <exception>
#include <unordered_map>

#include <cerrno>
#include <cstdio>
#include <cstdint>
#include <cstring>

#include <omp.h>
#include <zlib.h>
#include <sqlite3.h>

#include <fcntl.h>
#include <unistd.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <arpa/inet.h>
#include <netinet/in.h>

#define CLEN        (32 * 1024)
#define MMETA_SIZE  128
#define LL_HOST     "127.0.0.1"
#define LL_PORT     8000

using namespace std;

typedef std::vector<string> MC;
typedef std::unordered_map<int64_t, std::pair<int32_t, uint8_t>> KVResult;

string dbpath;
std::mutex sqlcon_mutex;
int avaliable_dbnum = 0;
#define sqlsetpath(path)    (dbpath = path)

static sqlite3* getsqlconn() {
    sqlite3 *con = NULL;
    sqlcon_mutex.lock();
    string dbp = dbpath + string("/db.") + std::to_string(avaliable_dbnum) + string(".db");
    sqlite3_open_v2(dbp.data(), &con, SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE, NULL);
    sqlite3_exec(con, "create table html (url_crc int primary key,html text,update_time char(20))", NULL, NULL, NULL);
    avaliable_dbnum++;
    sqlcon_mutex.unlock();
    return con;
}

struct LLKV {
    LLKV(string host, int port)
        :host(host), port(port)
    {
    }

  ~LLKV() { for (auto &i : this->sockpool) close(i); }

    int ping(int sockfd) {
        int r = 0;
        int32_t type = 0;
        int64_t data = 0;

        r = send(sockfd, &type, 4, 0);
        //cout<<"send: "<<r<<endl;
        //cout<<"prepare recv"<<endl;
        r = recv(sockfd, &data, 8, 0);
        //cout<<"recv: "<<r<<","<<data<<endl;

        int32_t pong = data & 0xffffffff;
        int32_t code = data >> 32;

        if (pong != 1 || code != 0)
            throw std::runtime_error("Connection Error.");

        return 0;
    }

    KVResult mget(std::vector<std::pair<int64_t/* crc */, uint32_t/* siteid */> > &keys) {
        //char *packet = new char[(keys.size() + 1) * 8];
        int r = 0;
        int16_t index = 0;
        char packet[32 * 1024] = { 0 };
        char inbuff[32 * 1024] = { 0 };

        int32_t type = 3;
        int32_t size = keys.size() * 8;

        memcpy(packet+index, &type, 4); index += 4;
        memcpy(packet+index, &size, 4); index += 4;

        for (auto i=keys.begin(); i != keys.end(); i++) {
            int64_t data = i->first | (uint64_t(i->second) << 48);
            //cout<<"crc: "<<i->first<<","<<"sid: "<<i->second<<", "<<data<<endl;
            memcpy(packet+index, &data, 8); index += 8;
        }

        KVResult ret;
        int sockfd = this->getsocket();
        int maxretry = 8;
        //int sockfd = this->spget();
    loop:
        //cout<<"prepare send"<<endl;
        r = send(sockfd, packet, index, 0);
        //cout<<"send: "<<r<<endl;
        //cout<<"prepare recv"<<endl;
        //cout<<"recv: "<<r<<endl;
        r = this->recvall(sockfd, inbuff, index);
        if (r < 0) {
            if (maxretry-- > 0) {
            //sockfd = this->spget();
                sockfd = this->getsocket();
                goto loop;
            }
            return ret;
        }

        for (int i=8; i<index; i+=8) {
            uint64_t data = *((uint64_t*)(inbuff+i));
            int32_t price = data & 0xffffffff;
            uint8_t stock = (data >> 32) != 0xffffffff ? 1 : 0;

            //cout<<"data: "<<data<<","<<"price: "<<price<<","<<"stock: "<<int(stock)<<endl;
            std::pair<int32_t, uint8_t> ps(price, stock);

            int64_t crc = keys[i/8 - 1].first;
            ret[crc] = ps;
        }

        //cout<<"close socket"<<endl;
        //this->spput(sockfd);
        close(sockfd);
        return ret;
    }

    string host;
    int port;

private:
    int getsocket() {
        int r = 0;
        struct sockaddr_in svaddr;
        svaddr.sin_family = AF_INET;
        svaddr.sin_port = htons(this->port);
        inet_pton(AF_INET, this->host.data(), &svaddr.sin_addr);

        int sockfd = socket(AF_INET, SOCK_STREAM, 0);
        //cout<<"sockfd: "<<sockfd<<endl;
        r = connect(sockfd, (struct sockaddr*)&svaddr, sizeof(svaddr));
        //cout<<"connect status: "<<r<<endl;
        //cout<<"connect finished"<<endl;
        ping(sockfd);
        return sockfd;
    }

    int recvall(int sockfd, char *buffer, ssize_t len) {
        int r = 0;
        int index = 0;
        while(index < len && (r = recv(sockfd, buffer+index, len, 0)) > 0) index += r;
        if (r < 0 || index != len) {
            cout<<"recv error: "<<index<<", "<<len<<", "<<errno<<endl;
            close(sockfd);
            return -1;
        }
        return 0;
  }

  int spget() {
    int fd = -1;
    spglock.lock();
    if (!sockpool.size())
      fd = this->getsocket();
    fd = sockpool.back();
    sockpool.pop_back();
    spglock.unlock();
    return fd;
  }

  void spput(int fd) {
    spplock.lock();
    sockpool.push_back(fd);
    spplock.unlock();
  }

  std::vector<int> sockpool;
  std::mutex spglock, spplock;
};

struct MEntry {
    uint32_t site_id;
    uint32_t date_time;
    int64_t crc;
    uint32_t offset;
    uint32_t size;
    char url[MMETA_SIZE - 24];
} __attribute__((packed));

struct MFile {
    typedef MEntry* iterator;

    MFile(int f) : buffer(NULL) {
        this->fd = f;

        this->buflen = lseek(this->fd, 0, SEEK_END);
        int i = lseek(this->fd, 0, SEEK_SET);

        this->buffer = (char*)mmap(NULL, this->buflen, PROT_READ, MAP_SHARED, fd, 0);

        // skip magic, version, n_entry
        this->magic = 0xCAFE;
        this->version = 1;

        char *buf = this->buffer + 8;
        this->entrys = *((uint32_t *)(buf));
        this->entry_size = *((uint32_t *)(buf + 4));
        this->eoffset = buf + 8 - this->buffer;
    }

    ~MFile() {
        munmap(this->buffer, this->buflen);
        close(fd);
    }

    inline const char* data(MEntry *m = NULL) {
        if (!m) return this->buffer;
        return this->buffer + m->offset;
    }

    inline iterator begin() { return iterator(this->buffer + this->eoffset); }
    inline iterator end() { return iterator(this->begin() + this->entrys); }

protected:
    iterator start;
    iterator finish;

private:
    int fd;

    int buflen;
    char* buffer;

    uint32_t magic;
    uint32_t version;
    uint32_t eoffset;
    uint32_t entrys;
    uint32_t entry_size;
};

struct MEntryReader {
    MEntryReader(MFile *m) {
        this->mfile = shared_ptr<MFile>(m);
        this->entry = this->mfile->begin();
    }

    MEntry* next() {
        if (!this->entry) return NULL;
        if (this->entry >= this->mfile->end()) return NULL;
        MFile::iterator i = this->entry;
        this->entry++;
        return i;
    }

    inline const char* data(MEntry *m = NULL) { return this->mfile->data(m); }

private:
    shared_ptr<MFile> mfile;
    MEntry *entry;
};

struct MERManager {
    MERManager(MC &c, string fp)
        : config(c), fpath(fp) {
        this->ereader = this->nextreader();
    }

    MEntry* next() {
        MEntry *m = this->ereader->next();
        if (!m) this->ereader = this->nextreader();
        if (!m && this->ereader) m = this->ereader->next();
        return m;
    }

    vector<MEntry*> current() {
        vector<MEntry*> v;
        MEntry *m = this->ereader->next();
        if (!m) this->ereader = this->nextreader();
        else v.push_back(m);
        while(this->ereader && (m = this->ereader->next())) v.push_back(m);
        //cout<<"read current group"<<endl;
        return v;
    }

    inline const char *data(MEntry *m = NULL) { return this->ereader->data(m); }

private:
    shared_ptr<MEntryReader> nextreader() {
    loop:
        if (!config.size()) return NULL;
        auto i = config.back();
        string p = fpath + string("/") + i;
        int fd = open(p.data(), O_RDONLY);
        if (fd > 0) cout<<"open: ";
        else cout<<"open failed: ";
        cout<<p.data()<<endl;
        config.pop_back();
        if (fd < 0) goto loop;
        return shared_ptr<MEntryReader>(new MEntryReader(new MFile(fd)));
    }

    MC config;
    shared_ptr<MEntryReader> ereader;
    string fpath;
};

static string iformat(std::pair<int32_t, uint8_t> &ps, const char *url) {
    int32_t price = ps.first;
    uint8_t stock = ps.second;

    char pbuf[64] = { 0 };
    std::sprintf(pbuf, "%.2f", double(price) / 100);

    string surl = string(url);
    auto us = surl.find("http");
    surl = surl.substr(us, surl.size() - us);

    return "<base href=" + surl + ">\n" + \
            "<Content-Type: application/octet-stream>\n" + \
            "<tt_price>" + pbuf + "</tt_price>" + \
            "<tt_stock>" + std::to_string(stock) + "</tt_stock>";
}

void dummytask(MC &cc, string fp, LLKV &ll, int limit=100000) {
    int r;

    int tid = omp_get_thread_num();

    //MEntry *m = NULL;
    MERManager manager(cc, fp);
    std::vector<MEntry*> current;

    sqlite3 *con = NULL;
    const char *cstmt = "replace into html (url_crc,html,update_time) values (?,?,?)";
    sqlite3_stmt *insert_stmt = NULL;

    uLongf hbuflen = 4*1024*1024, tbuflen = 21;
    char *hdbuf = new char[hbuflen];
    char *hsbuf = new char[hbuflen];
    char tbuf[tbuflen];
    char tmpbuf[hbuflen];
    //m = manager.next();

    int64_t filecount = 0;
    current = manager.current();

    while (current.size()) {
        std::vector<std::pair<int64_t, uint32_t>> keys;

    for (auto &i : current)
            keys.push_back(std::pair<int64_t, uint32_t>(int64_t(i->crc), uint32_t(i->site_id)));

    KVResult pscache = ll.mget(keys);
        cout<<"pscache.size: "<<pscache.size()<<endl;

    if (!filecount) con = getsqlconn();

    sqlite3_exec(con, "begin", NULL, NULL, NULL);
        for (auto &m : current) {
            //cout<<"in thread#"<<tid;
            // if (!m) {
            //  printf("m is None.\n");
            //  break;
            // }

            const char *b = manager.data();

            sqlite3_prepare_v2(con, cstmt, -1, &insert_stmt, NULL);

            //string qs = to_string(m->crc) + "-" + to_string(m->site_id);
            //cout<<std::setw(15)<<qs.data();

            auto ent = pscache.find(m->crc);
            std::pair<int32_t, uint8_t> ps;
            if (ent == pscache.end())
                ps = std::pair<int32_t, uint8_t>(-1, 0);
            else
                ps = ent->second;

            auto tti = iformat(ps, m->url);
            auto tsz = tti.size();
            memcpy(hsbuf, tti.data(), tti.size());
            memcpy(hsbuf + tsz, b + m->offset, m->size);

            uLongf hbl = hbuflen;

            r = compress((Bytef*)hdbuf, &hbl, (Bytef*)hsbuf, tsz + m->size);
            if (r != Z_OK)
                cout<<std::setw(20)<<"compress error: "<<r<<" "<<hbl<<","<<tsz+m->size<<endl;
            //else
            //  cout<<std::setw(20)<<"compressed size: "<<hbl<<endl;

            auto t = time(NULL);
            strftime(tbuf, tbuflen, "%Y-%m-%d %H:%M:%S", localtime(&t));

            sqlite3_bind_int(insert_stmt, 1, m->crc);
            sqlite3_bind_text(insert_stmt, 2, hdbuf, hbl+tti.size(), NULL);
            sqlite3_bind_text(insert_stmt, 3, tbuf, tbuflen, NULL);

            if ((r = sqlite3_step(insert_stmt) != SQLITE_DONE))
                cout<<"sql exec error: "<<r<<endl;

            sqlite3_finalize(insert_stmt);
            
            filecount++;
        }

    if (filecount >= limit) {
          sqlite3_exec(con, "commit", NULL, NULL, NULL);
          sqlite3_close(con);

      filecount = 0;
      con = NULL;
    }

    current = manager.current();
    }

    delete hdbuf;
    delete hsbuf;

  printf("dummytask return\n");
  if (!con) return;

  sqlite3_exec(con, "commit", NULL, NULL, NULL);
  sqlite3_close(con);
}

void globalrun(MC c, string fp, LLKV &ll, int limit = 100000, int workers = 8) {
    if (workers < 1 || !c.size()) return;
    int wclen = c.size() / workers;

    #pragma omp parallel for
    for (int i=1; i<=workers; i++) {
        MC cc;
        auto iter = c.begin() + (i-1) * wclen, niter = iter;
        if (i == workers) niter = c.end();
        else niter += wclen;
        std::copy(iter, niter, std::back_inserter(cc));
        dummytask(cc, fp, ll, limit);
    }
}

// f2s <config> <fpath> <dbpath> <worker_num> [start_dbnum]
int main(int argc, char *argv[]) {
    if (argc < 5) {
        cout<<"Usage: f2s <config> <fpath> <dbpath> <worker_num>"<<endl;
        return -1;
    }

    ifstream fconfig(argv[1], std::ifstream::in);
    string cn;
    MC config;
    while (std::getline(fconfig, cn))
        config.push_back(cn);

    sqlsetpath(argv[3]);

    //Tyrant tt(TT_HOST, TT_PORT);
    cout<<"init llkv client"<<endl;
    LLKV ll(LL_HOST, LL_PORT);
    cout<<"llkv client init finished."<<endl;

    int worker_num = stoi(argv[4]);
    if (worker_num < 1) {
        cout<<"bad worker number."<<endl;
        return -1;
    }

    if (argc == 6)
        avaliable_dbnum = stoi(argv[5]);

    cout<<"exec:globalrun"<<endl;
    globalrun(config, argv[2], ll, 100000, worker_num);

    return 0;
}
